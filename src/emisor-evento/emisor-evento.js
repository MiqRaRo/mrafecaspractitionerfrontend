import { LitElement, html } from 'lit-element';

class EmisorEvento extends LitElement {

  static get properties(){
      return {

      };
  }

  constructor() {
    super();
  }

  render (){
      return html`
        <div>Emisor-evento</div>
        <button @click="${this.sendEvent}">No pulsar</button>
      `;
  }

  sendEvent(e) {
    console.log("sendEvent");
    console.log(e);

    this.dispatchEvent(
      new CustomEvent(
        "test-event",
        {
          "detail": {
            "course": "TechU",
            "year": 2021
          }
        }
      )
    );
  }

}

customElements.define('emisor-evento', EmisorEvento);
