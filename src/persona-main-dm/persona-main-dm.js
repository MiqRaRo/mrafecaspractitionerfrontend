import { LitElement, html } from 'lit-element';

class PersonaMainDM extends LitElement {

  static get properties(){
    return{
      people : {type: Array}
    };
  }

  constructor() {
    super();

    this.fetchContacts();
  }

  /*connectedCallback() {
    super.connectedCallback();

    if (!this.people) {
      this.fetchContacts();
    }
  }*/

  fetchContacts() {
    this.people = [
      {
        name : "Monkey D. Luffy",
        yearsInCompany : 10,
        profile : "Lorem ipsum dolor sit amet, consectetur adipiscing elit",
        photo: {
          src: "./img/img.jpg",
          alt: '"Monkey D. Luffy"'
        }
      },
      {
        name : "Roronoa Zoro",
        yearsInCompany : 9,
        profile : "Sed iaculis quis mi id feugiat",
        photo: {
          src: "./img/img2.jpg",
          alt: "Roronoa Zoro"
        }
      },
      {
        name : "Nami",
        yearsInCompany : 8,
        profile : "Nam erat ex, rutrum et leo id, dignissim ultrices lorem",
        photo: {
          src: "./img/img3.jpg",
          alt: "Nami"
        }
      },
      {
        name : "Vinsmoke Sanji",
        yearsInCompany : 8,
        profile : "Curabitur elementum faucibus sodales",
        photo: {
          src: "./img/img4.jpg",
          alt: "Nami"
        }
      },
      {
        name : "Nico Robin",
        yearsInCompany : 4,
        profile : "Fusce viverra et odio nec venenatis",
        photo: {
          src: "./img/img5.jpg",
          alt: "Nami"
        }
      }

    ];
  }

  updated(changedProperties) {
    if(changedProperties.has("people")){
      console.log("Ha cambiado la propiedad people en persona-main-dm");

      this.dispatchEvent(new CustomEvent(
        "people-data-updated",{
          detail : {
            people : this.people
          }
        }
      ));
    }
  }

}

customElements.define('persona-main-dm', PersonaMainDM);
