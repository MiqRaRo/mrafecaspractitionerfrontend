import { LitElement, html } from 'lit-element';

class FichaPersona extends LitElement {

  static get properties(){
    return{
      name: {type: String},
      yearsInCompany: {type: Number},
      personInfo: {type: String}
    };
  }

  constructor() {
    super();
    this.name = 'Miquel Rafecas Romero';
    this.yearsInCompany = 4;

    if(this.yearsInCompany >= 7){
      this.personInfo = "lead";
    }else if (this.yearsInCompany >= 5){
      this.personInfo = "senior";
    }else if (this.yearsInCompany >= 3){
      this.personInfo = "team";
    }else{
      this.personInfo = "junior";
    }

  }

  updated(changedProperties) {
    console.log("updated");

    changedProperties.forEach((oldValue, propName) => {
      console.log("Propiedad " + propName + " cambia valor, antrior era " + oldValue);
    });

    if(changedProperties.has("name")){
      console.log("Propiedad name ha cambiado de valor, anterior era " + changedProperties.get("name") + " nuevo es " + this.name);
    }

    if(changedProperties.has("yearsInCompany")){
      console.log("Propiedad yearsInCompany ha cambiado de valor, anterior era " + changedProperties.get("yearsInCompany") + " nuevo es " + this.yearsInCompany);
      this.updatePersonInfo();
    }

  }

  render (){
      return html`
        <div>
          <label>Nombre completo: </label>
          <input type="text" id="fname" name="fname" value="${this.name}" @input="${this.updateName}"></input>
          <br />
          <label>Años en la empresa: </label>
          <input type="text" name="yearsInCompany" value="${this.yearsInCompany}" @input="${this.updateYearsInCompany}"></input>
          <br />
          <input type="text" value="${this.personInfo}" disabled></input>
          <br />
        </div>
      `;
  }

  updateName(e) {
    console.log("updateName, e: " +e);
    this.name = e.target.value;
  }

  updateYearsInCompany(e){
    console.log("updateYearsInCompany, e: " +e);
    this.yearsInCompany = e.target.value;
  }

  updatePersonInfo(){
    if(this.yearsInCompany >= 7){
      this.personInfo = "lead";
    }else if (this.yearsInCompany >= 5){
      this.personInfo = "senior";
    }else if (this.yearsInCompany >= 3){
      this.personInfo = "team";
    }else{
      this.personInfo = "junior";
    }
  }

}

customElements.define('ficha-persona', FichaPersona);
